export interface Survey {
  _id: string;
  name: string;
  start_date: string;
  end_date: string;
  questions: Questions;
  answers: Answers;
}

interface Questions {
  id: number;
  type: number;
  name: string;
  options: Options;
}

interface Options {
  id: number;
  name: string;
}

interface Answers {
  user_id: number;
  answer_date: string;
  questions: QuestionsAnswer;
}

interface QuestionsAnswer {
  id: number;
  answer: number;
}
