import { Component, OnInit } from '@angular/core';

import { SurveyService } from '../services/survey.service';
import { Survey } from '../survey';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css']
})
export class QuizComponent implements OnInit {

  survey: Survey;
  stt: number;

  constructor(
    private surveyService: SurveyService
  ) { }

  ngOnInit(): void {
    this.getSurveys();
  }

  private getSurveys() {
    this.surveyService.getSurvey().subscribe(survey => this.survey = survey);
  }

  genIndex(index = 1): number {
    this.stt = index + 1;
    return index;
  }

}
